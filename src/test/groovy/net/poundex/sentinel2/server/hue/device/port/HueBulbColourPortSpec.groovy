package net.poundex.sentinel2.server.hue.device.port

import net.poundex.sentinel2.server.env.value.ColourValue
import net.poundex.sentinel2.server.hue.HueTestData
import net.poundex.sentinel2.server.hue.client.HueApiResponse
import net.poundex.sentinel2.server.hue.client.HueBridgeClient
import spock.lang.Specification
import spock.lang.Subject

class HueBulbColourPortSpec extends Specification implements HueTestData {
	
	HueBridgeClient bridgeClient = Mock()
	
	@Subject
	HueBulbColourPort port = HueBulbColourPort.create(URI.create("hue://hue0/bulb/1/self"), bridgeClient, "1")
	
	void "Port ID is correct"() {
		expect:
		port.portId == URI.create("hue://hue0/bulb/1/colour")
	}
	
	void "Read current colour value from Hue bulb"() {
		given:
		bridgeClient.getEverything() >> new HueApiResponse([
				"1": lightInfo {
					it.state.bri = 254
					it.state.hue = 65535
					it.state.sat = 254
				}
		])

		expect:
		with(port.readValue()) {
			brightness == 254
			hue == 65535
			saturation == 254
		}
	}
	
	void "Write power value to Hue bulb"() {
		when:
		port.writeValue(new ColourValue(254, 65535, 254))

		then:
		1 * bridgeClient.setLightState("1", [
				on: true,
				bri: 254,
				hue: 65535,
				sat: 254
		])
	}
}
