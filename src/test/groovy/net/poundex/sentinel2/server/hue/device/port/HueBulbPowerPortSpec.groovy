package net.poundex.sentinel2.server.hue.device.port

import net.poundex.sentinel2.server.env.value.BooleanValue
import net.poundex.sentinel2.server.hue.HueTestData
import net.poundex.sentinel2.server.hue.client.HueApiResponse
import net.poundex.sentinel2.server.hue.client.HueBridgeClient
import spock.lang.Specification
import spock.lang.Subject

class HueBulbPowerPortSpec extends Specification implements HueTestData {
	
	HueBridgeClient bridgeClient = Mock()
	
	@Subject
	HueBulbPowerPort port = HueBulbPowerPort.create(URI.create("hue://hue0/bulb/1/self"), bridgeClient, "1")
	
	void "Port ID is correct"() {
		expect:
		port.portId == URI.create("hue://hue0/bulb/1/power")
	}
	
	void "Read current power value from Hue bulb"() {
		given:
		bridgeClient.getEverything() >>> [
				new HueApiResponse([
						"1": lightInfo {
							it.state.on = true
						}
				]),
				new HueApiResponse([
				        "1": lightInfo {
					        it.state.on = false
				        }
				])
		]

		expect:
		port.readValue().value
		! port.readValue().value
	}
	
	void "Write power value to Hue bulb"() {
		when:
		port.writeValue(BooleanValue.TRUE)
		
		then:
		1 * bridgeClient.setLightState("1", [on: true])
		
		when:
		port.writeValue(BooleanValue.FALSE)

		then:
		1 * bridgeClient.setLightState("1", [on: false])
	}
}
