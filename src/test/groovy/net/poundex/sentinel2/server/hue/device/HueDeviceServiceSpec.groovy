package net.poundex.sentinel2.server.hue.device

import net.poundex.sentinel2.server.device.DriverEvents
import net.poundex.sentinel2.server.hue.bridge.HueBridge
import net.poundex.sentinel2.server.hue.bridge.HueBridgeRepository
import net.poundex.sentinel2.server.hue.client.HueApiResponse
import net.poundex.sentinel2.server.hue.client.HueBridgeClient
import net.poundex.sentinel2.server.hue.client.HueBridgeClientFactory
import org.springframework.core.env.Environment
import reactor.core.publisher.Flux
import spock.lang.Specification
import spock.lang.Subject

class HueDeviceServiceSpec extends Specification {
	
	DriverEvents driverEvents = Mock()
	HueBridgeRepository repository = Stub()
	Environment springEnvironment = Stub()
	HueBridgeClientFactory hueBridgeClientFactory = Stub()
	
	@Subject
	HueDeviceService hueDeviceService = new HueDeviceService(driverEvents, repository, springEnvironment, hueBridgeClientFactory)
	
	void "Register all devices"() {
		given:
		repository.findAll() >> Flux.just(HueBridge.builder()
				.name("hue0")
				.address("1.2.3.4")
				.build())
		
		springEnvironment.getRequiredProperty("sentinel.hue.hue0.username", String.class) >> "some-username"

		hueBridgeClientFactory.createClient("1.2.3.4", "some-username") >> Stub(HueBridgeClient) {
			getEverything() >> new HueApiResponse(Map.of(
					"1", new HueApiResponse.LightInfo(new HueApiResponse.LightInfo.LightState(),
					"", "Basic Bulb", "", "", "", "",
					new HueApiResponse.LightInfo.Capabilities(new HueApiResponse.LightInfo.Capabilities.Control())),

					"2", new HueApiResponse.LightInfo(new HueApiResponse.LightInfo.LightState(),
					"", "Colour Bulb", "", "", "", "",
					new HueApiResponse.LightInfo.Capabilities(
							new HueApiResponse.LightInfo.Capabilities.Control(0, 0, "C")))))
		}
		
		when:
		hueDeviceService.start()
		
		then:
		1 * driverEvents.deviceAdded(_ as HueBridgeDevice)
		2 * driverEvents.deviceAdded(_ as HueBulbDevice)
	}
}
