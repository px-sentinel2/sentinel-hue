package net.poundex.sentinel2.server.hue.device.port

import net.poundex.sentinel2.server.env.value.ColourTemperatureValue
import net.poundex.sentinel2.server.hue.HueTestData
import net.poundex.sentinel2.server.hue.client.HueApiResponse
import net.poundex.sentinel2.server.hue.client.HueBridgeClient
import spock.lang.Specification
import spock.lang.Subject

class HueBulbColourTemperaturePortSpec extends Specification implements HueTestData {
	
	HueBridgeClient bridgeClient = Mock()
	
	@Subject
	HueBulbColourTemperaturePort port = 
			HueBulbColourTemperaturePort.create(URI.create("hue://hue0/bulb/1/self"), bridgeClient, "1")
	
	void "Port ID is correct"() {
		expect:
		port.portId == URI.create("hue://hue0/bulb/1/ct")
	}
	
	void "Read current colour temp value from Hue bulb"() {
		given:
		bridgeClient.getEverything() >> new HueApiResponse([
				"1": lightInfo {
					it.state.bri = 254
					it.state.ct = 153
				}
		])

		expect:
		with(port.readValue()) {
			brightness == 254
			temperature == 153
		}
	}
	
	void "Write colour temp value to Hue bulb"() {
		when:
		port.writeValue(new ColourTemperatureValue(254, 153))

		then:
		1 * bridgeClient.setLightState("1", [
				bri: 254,
				ct: 153
		])
	}

}
