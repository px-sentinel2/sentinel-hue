package net.poundex.sentinel2.server.hue

import net.poundex.sentinel2.server.hue.client.HueApiResponse

import java.util.function.Consumer

trait HueTestData {

	HueApiResponse.LightInfo lightInfo(Consumer<HueApiResponse.LightInfo> consumer) {
		HueApiResponse.LightInfo li = new HueApiResponse.LightInfo(
				new HueApiResponse.LightInfo.LightState(
						true,
						0,
						0,
						0,
						[0] as double[],
						0,
						null,
						null,
						null,
						false),
				null,
				null,
				null,
				null,
				null,
				null,
				null)
		consumer.accept(li)
		return li
	}
}