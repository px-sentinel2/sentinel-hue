package net.poundex.sentinel2.server.hue.device;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.sentinel2.server.device.Driver;
import net.poundex.sentinel2.server.device.DriverEvents;
import net.poundex.sentinel2.server.hue.bridge.HueBridge;
import net.poundex.sentinel2.server.hue.bridge.HueBridgeRepository;
import net.poundex.sentinel2.server.hue.client.HueBridgeClient;
import net.poundex.sentinel2.server.hue.client.HueBridgeClientFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.net.URI;

@Service
@RequiredArgsConstructor
@Slf4j
class HueDeviceService implements Driver {

	private final DriverEvents driverEvents;
	private final HueBridgeRepository hueBridgeRepository;
	private final Environment springEnvironment;
	private final HueBridgeClientFactory hueBridgeClientFactory;

	@Override
	public String getName() {
		return "hue";
	}

	@Override
	public void start() {
		// TODO: block
		hueBridgeRepository.findAll().collectList().block().forEach(this::startBridge);
	}
	
	private void startBridge(HueBridge bridge) {
		log.info("Configuring Hue Bridge {}@{}", bridge.getName(), bridge.getAddress());

		HueBridgeClient client = hueBridgeClientFactory.createClient(bridge.getAddress(),
				springEnvironment.getRequiredProperty(
						String.format("sentinel.hue.%s.username", bridge.getName()), String.class));

		HueBridgeDevice bridgeDevice = HueBridgeDevice.builder()
				.deviceId(URI.create(String.format("hue://%s/self", bridge.getName())))
				.build();
		driverEvents.deviceAdded(bridgeDevice);

		client.getEverything().getLights().forEach((bulbId, lightInfo) ->
				driverEvents.deviceAdded(HueBulbDevice.create(
						bridge,
						bridgeDevice,
						client,
						bulbId,
						lightInfo)));
	}
}
