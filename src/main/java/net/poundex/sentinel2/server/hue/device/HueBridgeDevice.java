package net.poundex.sentinel2.server.hue.device;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import net.poundex.sentinel2.server.device.Device;
import net.poundex.sentinel2.server.device.port.DevicePort;
import net.poundex.sentinel2.server.env.value.Value;

import java.net.URI;
import java.util.Optional;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
class HueBridgeDevice implements Device {
	private final URI deviceId;

	@Override
	public <VT extends Value<VT>> Optional<DevicePort<VT>> getPort(URI path) {
		return Optional.empty();
	}
}
