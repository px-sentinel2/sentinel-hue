package net.poundex.sentinel2.server.hue.client;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HueApiResponse {
	private Map<String, LightInfo> lights;

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class LightInfo {
		private LightState state;

		@Data
		@NoArgsConstructor
		@AllArgsConstructor
		public static class LightState {
			private boolean on;
			private int bri;
			private int hue;
			private int sat;
			private double[] xy;
			private int ct;
			private String alert;
			private String colormode;
			private String mode;
			private boolean reachable;
		}

		private String type;
		private String name;
		private String modelid;
		private String manufacturername;
		private String productname;
		private String uniqueid;
		private Capabilities capabilities;
		
		@Data
		@NoArgsConstructor
		@AllArgsConstructor
		public static class Capabilities {
			private Control control;

			@Data
			@NoArgsConstructor
			@AllArgsConstructor
			public static class Control {
				private int mindimlevel;
				private int maxlumen;
				private String colorgamuttype;
			}
		}
	}
}
