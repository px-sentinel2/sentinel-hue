package net.poundex.sentinel2.server.hue.device.port;

import lombok.*;
import net.poundex.sentinel2.server.device.port.DevicePort;
import net.poundex.sentinel2.server.device.port.ReadWriteDevicePort;
import net.poundex.sentinel2.server.env.value.BooleanValue;
import net.poundex.sentinel2.server.hue.client.HueBridgeClient;

import java.net.URI;
import java.util.Map;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
@ToString(onlyExplicitlyIncluded = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class HueBulbPowerPort implements ReadWriteDevicePort<BooleanValue> {
	
	public static HueBulbPowerPort create(URI bulbDeviceId, HueBridgeClient bridgeClient, String bulbId) {
		return new HueBulbPowerPort(bulbDeviceId.resolve(DevicePort.POWER), bridgeClient, bulbId);
	}

	@Getter
	@ToString.Include
	@EqualsAndHashCode.Include
	private final URI portId;
	private final HueBridgeClient bridgeClient;
	private final String bulbId;

	@Override
	public BooleanValue readValue() {
		return new BooleanValue(bridgeClient.getEverything()
				.getLights()
				.get(bulbId)
				.getState()
				.isOn());
	}

	@Override
	public void writeValue(BooleanValue deviceValue) {
		bridgeClient.setLightState(bulbId, Map.of("on", deviceValue.value()));
	}
}
