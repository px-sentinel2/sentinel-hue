package net.poundex.sentinel2.server.hue.client;

import feign.Param;
import feign.RequestLine;

import java.util.List;
import java.util.Map;

public interface HueBridgeClient {
	@RequestLine("GET /")
	HueApiResponse getEverything();

	@RequestLine("PUT /lights/{bulbId}/state")
	List<Map<String, Object>> setLightState(@Param("bulbId") String bulbId, Map<String, Object> state);
}
