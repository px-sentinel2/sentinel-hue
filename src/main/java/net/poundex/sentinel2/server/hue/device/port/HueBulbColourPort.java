package net.poundex.sentinel2.server.hue.device.port;

import lombok.*;
import net.poundex.sentinel2.server.device.port.ReadWriteDevicePort;
import net.poundex.sentinel2.server.env.value.ColourValue;
import net.poundex.sentinel2.server.hue.client.HueApiResponse;
import net.poundex.sentinel2.server.hue.client.HueBridgeClient;

import java.net.URI;
import java.util.Map;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
@ToString(onlyExplicitlyIncluded = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class HueBulbColourPort implements ReadWriteDevicePort<ColourValue> {
	
	public static HueBulbColourPort create(URI bulbDeviceId, HueBridgeClient bridgeClient, String bulbId) {
		return new HueBulbColourPort(bulbDeviceId.resolve("colour"), bridgeClient, bulbId);
	}

	@Getter
	@ToString.Include
	@EqualsAndHashCode.Include
	private final URI portId;
	private final HueBridgeClient bridgeClient;
	private final String bulbId;

	@Override
	public ColourValue readValue() {
		HueApiResponse.LightInfo.LightState state = bridgeClient.getEverything()
				.getLights()
				.get(bulbId)
				.getState();
		return new ColourValue(state.getBri(), state.getHue(), state.getSat());
	}

	@Override
	public void writeValue(ColourValue value) {
		bridgeClient.setLightState(bulbId, Map.of(
				"on", true,
				"bri", value.getBrightness(),
				"hue", value.getHue(),
				"sat", value.getSaturation()
		));
	}
}
