package net.poundex.sentinel2.server.hue.client;

import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class HueBridgeClientConfiguration {
	@Bean
	public HueBridgeClientFactory hueBridgeClientFactory() {
		return (address, username) -> new SerialHueBridgeClientProxy(Feign.builder()
				.encoder(new JacksonEncoder())
				.decoder(new JacksonDecoder())
				.target(HueBridgeClient.class, String.format("http://%s/api/%s", address, username)));
	}
}
