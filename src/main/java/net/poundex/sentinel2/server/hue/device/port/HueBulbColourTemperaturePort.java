package net.poundex.sentinel2.server.hue.device.port;

import lombok.*;
import net.poundex.sentinel2.server.device.port.ReadWriteDevicePort;
import net.poundex.sentinel2.server.env.value.ColourTemperatureValue;
import net.poundex.sentinel2.server.hue.client.HueApiResponse;
import net.poundex.sentinel2.server.hue.client.HueBridgeClient;

import java.net.URI;
import java.util.Map;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
@ToString(onlyExplicitlyIncluded = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class HueBulbColourTemperaturePort implements ReadWriteDevicePort<ColourTemperatureValue> {
	
	public static HueBulbColourTemperaturePort create(URI bulbDeviceId, HueBridgeClient bridgeClient, String bulbId) {
		return new HueBulbColourTemperaturePort(bulbDeviceId.resolve("ct"), bridgeClient, bulbId);
	}

	@Getter
	@ToString.Include
	@EqualsAndHashCode.Include
	private final URI portId;
	private final HueBridgeClient bridgeClient;
	private final String bulbId;

	@Override
	public ColourTemperatureValue readValue() {
		HueApiResponse.LightInfo.LightState state = bridgeClient.getEverything()
				.getLights()
				.get(bulbId)
				.getState();
		return new ColourTemperatureValue(state.getBri(), state.getCt());
	}

	@Override
	public void writeValue(ColourTemperatureValue value) {
		bridgeClient.setLightState(bulbId, Map.of(
				"bri", value.getBrightness(),
				"ct", value.getTemperature()
		));
	}
}
