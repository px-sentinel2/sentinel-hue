package net.poundex.sentinel2.server.hue.device;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import net.poundex.sentinel2.server.device.Device;
import net.poundex.sentinel2.server.device.port.DevicePort;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.hue.bridge.HueBridge;
import net.poundex.sentinel2.server.hue.client.HueApiResponse;
import net.poundex.sentinel2.server.hue.client.HueBridgeClient;
import net.poundex.sentinel2.server.hue.device.port.HueBulbColourPort;
import net.poundex.sentinel2.server.hue.device.port.HueBulbColourTemperaturePort;
import net.poundex.sentinel2.server.hue.device.port.HueBulbPowerPort;
import org.springframework.util.StringUtils;

import java.net.URI;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class HueBulbDevice implements Device {
	
	@SuppressWarnings("unchecked")
	public static HueBulbDevice create(
			HueBridge bridge, 
			HueBridgeDevice bridgeDevice, 
			HueBridgeClient bridgeClient, 
			String bulbId, 
			HueApiResponse.LightInfo lightInfo) {

		URI bulbDeviceId = bridgeDevice.getDeviceId().resolve(HueBridge.bulb(bulbId));
		return new HueBulbDevice(bulbDeviceId, Map.ofEntries(Stream.of(
						Map.entry(DevicePort.POWER, HueBulbPowerPort.create(bulbDeviceId, bridgeClient, bulbId)),
						Map.entry(DevicePort.COLOUR_TEMPERATURE, HueBulbColourTemperaturePort.create(bulbDeviceId, bridgeClient, bulbId)),
						StringUtils.hasText(lightInfo.getCapabilities().getControl().getColorgamuttype())
								? Map.entry(DevicePort.COLOUR, HueBulbColourPort.create(bulbDeviceId, bridgeClient, bulbId))
								: null)
				.filter(Objects::nonNull)
				.toArray(Map.Entry[]::new)));
	}
	
	private final URI deviceId;
	private final Map<URI, DevicePort<?>> ports;

	@SuppressWarnings("unchecked")
	@Override
	public <VT extends Value<VT>> Optional<DevicePort<VT>> getPort(URI path) {
		return Optional.ofNullable((DevicePort<VT>) ports.get(path));
	}
}
