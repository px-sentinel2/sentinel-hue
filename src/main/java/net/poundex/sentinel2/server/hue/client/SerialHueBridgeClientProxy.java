package net.poundex.sentinel2.server.hue.client;

import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RequiredArgsConstructor
class SerialHueBridgeClientProxy implements HueBridgeClient {
	
	private final ExecutorService executor = Executors.newSingleThreadExecutor();
	
	private final HueBridgeClient delegate;
	
	@Override
	public HueApiResponse getEverything() {
		return Try.of(() -> executor.submit(delegate::getEverything).get()).get();
	}

	@Override
	public List<Map<String, Object>> setLightState(String bulbId, Map<String, Object> state) {
		return Try.of(() -> executor.submit(() -> delegate.setLightState(bulbId, state)).get())
				.andThenTry(() -> Thread.sleep(130))
				.get();
	}
}
