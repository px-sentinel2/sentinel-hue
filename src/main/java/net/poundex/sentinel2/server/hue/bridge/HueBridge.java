package net.poundex.sentinel2.server.hue.bridge;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import net.poundex.sentinel2.SentinelObject;
import net.poundex.sentinel2.server.Hardware;

import java.net.URI;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
public class HueBridge implements SentinelObject, Hardware {
	
	public static String bulb(String bulbId) {
		return String.format("bulb/%s/self", bulbId);
	}
	
	private final String id;
	private final String name;
	private final String address;

	@Override
	public URI getDeviceId() {
		return URI.create(String.format("hue://%s/self", name));
	}
}
