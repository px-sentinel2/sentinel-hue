package net.poundex.sentinel2.server.hue.bridge;

import net.poundex.sentinel2.ReadOnlyRepository;
import reactor.core.publisher.Mono;

public interface HueBridgeRepository extends ReadOnlyRepository<HueBridge> {
	Mono<HueBridge> create(String name, String address);
}
