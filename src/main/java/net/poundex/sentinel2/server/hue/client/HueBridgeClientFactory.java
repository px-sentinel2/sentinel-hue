package net.poundex.sentinel2.server.hue.client;

public interface HueBridgeClientFactory {
	HueBridgeClient createClient(String address, String username);
}
